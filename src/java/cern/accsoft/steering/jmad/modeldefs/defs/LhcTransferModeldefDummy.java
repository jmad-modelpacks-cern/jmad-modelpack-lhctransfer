/**
 * Copyright (c) 2015 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.defs;

/**
 * Just one java class, so that cmmnbuild sees a source file and allows to release the package.
 * 
 * @author delph
 */
public class LhcTransferModeldefDummy {
    /* Nothing to do here */
}
