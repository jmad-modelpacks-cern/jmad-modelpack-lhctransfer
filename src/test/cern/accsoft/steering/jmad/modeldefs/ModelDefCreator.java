package cern.accsoft.steering.jmad.modeldefs;

import java.io.File;

import org.apache.log4j.BasicConfigurator;

import cern.accsoft.steering.jmad.modeldefs.defs.R2015Ti2LhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2015Ti2ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2015Ti8LhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2015Ti8ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2017Ti2LhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2017Ti2ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2017Ti8LhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2017Ti8ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2018Q26Ti2LhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2018Q26Ti8LhcModelDefinitionFactory;

import cern.accsoft.steering.jmad.modeldefs.defs.R2018Q22Ti2LhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.defs.R2018Q22Ti2ModelDefinitionFactory;

import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.io.ModelDefinitionPersistenceService;
import cern.accsoft.steering.jmad.modeldefs.io.impl.ModelDefinitionUtil;
import cern.accsoft.steering.jmad.modeldefs.io.impl.XmlModelDefinitionPersistenceService;
import cern.accsoft.steering.jmad.util.xml.PersistenceServiceException;

public class ModelDefCreator {

    public final static void main(String[] args) {

        String destPath = "";
        if (args.length > 0) {
            destPath = args[0];
        }

        BasicConfigurator.configure();

        ModelDefinitionFactory[] factories = new ModelDefinitionFactory[] { //
                new R2015Ti2LhcModelDefinitionFactory(), new R2015Ti8LhcModelDefinitionFactory(),
                new R2015Ti2ModelDefinitionFactory(), new R2015Ti8ModelDefinitionFactory(),
                new R2017Ti2LhcModelDefinitionFactory(), new R2017Ti8LhcModelDefinitionFactory(),
                new R2017Ti2ModelDefinitionFactory(), new R2017Ti8ModelDefinitionFactory(),
                new R2018Q26Ti2LhcModelDefinitionFactory(), new R2018Q26Ti8LhcModelDefinitionFactory(),
                new R2018Q22Ti2LhcModelDefinitionFactory(), new R2018Q22Ti2ModelDefinitionFactory()};

        ModelDefinitionPersistenceService service = new XmlModelDefinitionPersistenceService();

        for (ModelDefinitionFactory factory : factories) {
            JMadModelDefinition modelDefinition = factory.create();
            String fileName = ModelDefinitionUtil.getProposedXmlFileName(modelDefinition);
            String filePath;
            if (destPath.length() > 0) {
                filePath = destPath + "/" + fileName;
            } else {
                filePath = "src/java/cern/accsoft/steering/jmad/modeldefs/defs/" + fileName;
            }
            File file = new File(filePath);
            System.out.println("Writing file '" + file.getAbsolutePath() + "'.");
            try {
                service.save(modelDefinition, file);
            } catch (PersistenceServiceException e) {
                System.out.println("Could not save model definition to file '" + file.getAbsolutePath());
                e.printStackTrace();
            }
        }

    }
}
