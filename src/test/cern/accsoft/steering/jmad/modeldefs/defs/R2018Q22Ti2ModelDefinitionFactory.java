/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.factory.BeamFactory;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * This class is the actual model configuration for the TI2 transfer line.
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 */
public class R2018Q22Ti2ModelDefinitionFactory extends AbstractR2017Ti2Factory implements ModelDefinitionFactory {

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TI2 Q22 2018");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setResourceOffset("lhctransfer/r2017/ti2");
        modelDefinition.setModelPathOffsets(offsets);

        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/ti2.seq", ModelFileLocation.RESOURCE));

        /*
         * OPTICS
         */
        OpticsDefinition opticsDefinition = new OpticsDefinitionImpl("LHCB1Transfer-Q22-2017", new ModelFile[] {
                new CallableModelFileImpl("strength/TI2_Q22.str", ModelFileLocation.RESOURCE, ParseType.STRENGTHS) });
        modelDefinition.setDefaultOpticsDefinition(opticsDefinition);

        /*
         * SEQUENCE
         */

        /* NOTE: sequenceName must correspond to the name in .seq - file! */
        SequenceDefinitionImpl ti2 = new SequenceDefinitionImpl("ti2", BeamFactory.createDefaultLhcBeam());
        modelDefinition.setDefaultSequenceDefinition(ti2);
        RangeDefinitionImpl ti2range = new RangeDefinitionImpl(ti2, "ALL", createTi2Twiss());
        ti2.setDefaultRangeDefinition(ti2range);

        // ti2range.addResponeElementNameRegexp("BPCK.*");
        // ti2range.addResponeElementNameRegexp("BPMI.*");
        /* XXX invert the X-bpms */
        ti2range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MDL.*", JMadPlane.H));
        ti2range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MDL.*", JMadPlane.V));
        ti2range.addPostUseFile(new CallableModelFileImpl("set-errors-q22.madx", ModelFileLocation.RESOURCE));

        return modelDefinition;
    }

}
