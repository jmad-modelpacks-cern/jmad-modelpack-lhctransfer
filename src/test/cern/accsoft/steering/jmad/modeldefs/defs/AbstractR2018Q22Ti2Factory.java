/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;

/**
 * The model for Q22 
 * 
 * @author jwenning
 */
public class AbstractR2018Q22Ti2Factory {

    /**
     * Twiss initial conditions as provided by Christoph Hessler
     */
    protected final TwissInitialConditionsImpl createTi2Twiss() {
        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl("ti2-twiss");

        twiss.setDeltap(0.0);
        twiss.setBetx(23.71848128);
        twiss.setAlfx(0.57835100 );
        twiss.setDx(0.45429688);
        twiss.setDpx(-0.04567687);
        twiss.setBety(120.46933981);
        twiss.setAlfy(-2.92789309);
        twiss.setDy(0.0);
        twiss.setDpy(0.0);

        /*
         * Q22 (MSE as in machine) - Q26 position at extraction (SPS)
         */
        twiss.setX(0.28137398 - 0.277702013);
        twiss.setPx(0.00869600 - 0.008874135741);
        return twiss;

    }

}