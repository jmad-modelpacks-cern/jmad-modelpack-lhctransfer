/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;

/**
 * @author kfuchsbe
 */
public class AbstractR2017Ti8Factory {

    /**
     * Twiss initial conditions as provided by Francesco, 2017-05-06, Q20 optics as designed:
     * <p>
     * betx = 27.93108087 , alfx = 0.65054900, bety = 120.05843813, alfy =-2.70513448, dx= -0.58140484, dpx= 0.01446870;
     */
    protected final TwissInitialConditionsImpl createTi8Twiss() {
        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl("ti8-twiss");

        twiss.setDeltap(0.000);
        twiss.setBetx(27.93108087);
        twiss.setAlfx(0.65054900);
        twiss.setDx(-0.58140484);
        twiss.setDpx(0.01446870);
        twiss.setBety(120.05843813);
        twiss.setAlfy(-2.70513448);
        twiss.setDy(0.0);
        twiss.setDpy(0.0);

        /*
         * Q20 postion - Q26 position, relative to nominal axis at SPS extraction "TI8$START"
         */
        twiss.setX(0.2669019917 - 0.2643833447);
        twiss.setPx(0.01120541487 - 0.01140006945);

        return twiss;

    }

}