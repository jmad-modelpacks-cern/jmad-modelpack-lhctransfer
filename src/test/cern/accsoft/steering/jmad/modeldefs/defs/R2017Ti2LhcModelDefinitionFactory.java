/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.NONE;
import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;
import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.RESOURCE;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.factory.BeamFactory;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * The model definition for Ti2+LHC sector 23
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 */
public class R2017Ti2LhcModelDefinitionFactory extends AbstractR2017Ti2Factory implements ModelDefinitionFactory {

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TI2-LHC 2017");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setResourceOffset("lhctransfer/r2017");
        modelDefinition.setModelPathOffsets(offsets);

        modelDefinition.addInitFile(new CallableModelFileImpl("ti2/sequence/ti2.seq", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("lhc/sequence/lhc2017.seq", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("ti2/join-ti2-lhc.madx", ModelFileLocation.RESOURCE));

        OpticsDefinition opticsDefinition = new OpticsDefinitionImpl("LHCB1Transfer-Q20-ATS-2017",
                new ModelFile[] { new CallableModelFileImpl("ti2/strength/TI2_Q20.str", RESOURCE, STRENGTHS),
                        new CallableModelFileImpl("lhc/strength/ats_11m_fixQ8L4.madx", RESOURCE, STRENGTHS),
                        new CallableModelFileImpl("ti2/set_lhc_flags_to_zero.madx", RESOURCE, NONE) });
        modelDefinition.setDefaultOpticsDefinition(opticsDefinition);

        /* NOTE: sequenceName must correspond to the name in .seq - file! */
        SequenceDefinitionImpl ti2LhcSeqDef = new SequenceDefinitionImpl("ti2lhcb1",
                BeamFactory.createDefaultLhcBeam());
        modelDefinition.addSequenceDefinition(ti2LhcSeqDef);
        modelDefinition.setDefaultSequenceDefinition(ti2LhcSeqDef);

        RangeDefinitionImpl b1range = new RangeDefinitionImpl(ti2LhcSeqDef, "ALL", createTi2Twiss());
        ti2LhcSeqDef.setDefaultRangeDefinition(b1range);

        /* invert first correctors */
        b1range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MDLH.*", JMadPlane.H));
        b1range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MDLV.*", JMadPlane.V));

        b1range.addPostUseFile(new CallableModelFileImpl("ti2/set-errors.madx", ModelFileLocation.RESOURCE));

        return modelDefinition;

    }

}
