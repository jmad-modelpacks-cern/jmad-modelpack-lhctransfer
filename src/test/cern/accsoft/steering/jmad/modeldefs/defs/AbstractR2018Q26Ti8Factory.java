/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;

/**
 * @author kfuchsbe
 */
public class AbstractR2018Q26Ti8Factory {

    /**
     *  2018 optics version initial conditions (C. Hessler), identical to the 2009 commissioning values
     */
    protected final TwissInitialConditionsImpl createTi8Twiss() {
        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl("ti8-twiss");

        /*
        ! betx = 17.06248574,alfx =0.4588913839,
        ! bety=124.8219205, alfy=-3.444554843,
        ! dx=  -0.2527900995,dpx=  0.00334144588;
         */
        
        twiss.setDeltap(0.000);
        twiss.setBetx(17.06248574);
        twiss.setAlfx(0.4588913839);
        twiss.setDx(-0.2527900995);
        twiss.setDpx(0.00334144588);
        twiss.setBety(124.8219205);
        twiss.setAlfy(-3.444554843);
        twiss.setDy(0.0);
        twiss.setDpy(0.0);

        /*
         * Q20 postion - Q26 position, relative to nominal axis at SPS extraction "TI8$START"
         */
        twiss.setX(0.0);
        twiss.setPx(0.0);

        return twiss;

    }

}