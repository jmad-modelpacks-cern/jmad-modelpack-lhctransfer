/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.factory.BeamFactory;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * This class is the actual model configuration for the TI2 transfer line.
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 */
public class R2015Ti2ModelDefinitionFactory implements ModelDefinitionFactory {

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TI2 2015");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setResourceOffset("lhctransfer/r2015/ti2");
        modelDefinition.setModelPathOffsets(offsets);

        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/ti2.seq", ModelFileLocation.RESOURCE));

        /*
         * OPTICS
         */
        OpticsDefinition opticsDefinition = new OpticsDefinitionImpl(
                "LHCB1Transfer-Q20-2015",
                new ModelFile[] { new CallableModelFileImpl("strength/ti2.str", ModelFileLocation.RESOURCE, ParseType.STRENGTHS) });
        modelDefinition.setDefaultOpticsDefinition(opticsDefinition);

        /*
         * SEQUENCE
         */

        /* NOTE: sequenceName must correspond to the name in .seq - file! */
        SequenceDefinitionImpl ti2 = new SequenceDefinitionImpl("ti2", BeamFactory.createDefaultLhcBeam());
        modelDefinition.setDefaultSequenceDefinition(ti2);
        RangeDefinitionImpl ti2range = new RangeDefinitionImpl(ti2, "ALL", createTi2Twiss());
        ti2.setDefaultRangeDefinition(ti2range);

        // ti2range.addResponeElementNameRegexp("BPCK.*");
        // ti2range.addResponeElementNameRegexp("BPMI.*");
        /* XXX invert the X-bpms */
        ti2range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MDL.*", JMadPlane.H));
        ti2range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MDL.*", JMadPlane.V));
        ti2range.addPostUseFile(new CallableModelFileImpl("set-errors-b2-b3.madx", ModelFileLocation.RESOURCE));

        return modelDefinition;
    }

    /**
     * Twiss initial conditions for transferline Ti2
     */
    private final TwissInitialConditionsImpl createTi2Twiss() {
        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl("ti2-twiss");

//        twiss_start_Q20: beta0, betx=27.77906807,bety=120.39920690,
//                alfx=0.63611880,alfy=-2.70621900,
//                dx=-0.59866300,dpx=0.01603536;

        /*
         * original values
         */

        twiss.setDeltap(0.0);
        twiss.setBetx(27.77906807);
        twiss.setAlfx(0.63611880);
        twiss.setDx(-0.59866300);
        twiss.setDpx(0.01603536);
        twiss.setBety(120.39920690);
        twiss.setAlfy(-2.70621900);
        twiss.setDy(0.0);
        twiss.setDpy(0.0);


        return twiss;

    }

}
