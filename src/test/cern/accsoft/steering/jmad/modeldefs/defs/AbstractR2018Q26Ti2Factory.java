/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;

/**
 * The model for Q26 (2018 ion) - still needs to update the initial conditions
 * 
 * @author jwenning
 */
public class AbstractR2018Q26Ti2Factory {

    /**
     *  2018 optics version initial conditions (C. Hessler), identical to the 2009 commissioning values
     *      
     * Twiss initial conditions as provided by malika on Oct, 20 2009 betx= 17.02748544, alfx= 0.4583574683,
     * bety=123.9323528, alfy=-3.422196857, dx = -0.3408152943, dpx = 0.01307653962;
     */
    protected final TwissInitialConditionsImpl createTi2Twiss() {
        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl("ti2-twiss");

        /*
           betx= 17.02748544,   alfx= 0.4583574683,
           bety=123.9323528,    alfy=-3.422196857,
           dx  = -0.3408152943, dpx = 0.01307653962;
        */
        
        twiss.setDeltap(0.0);
        twiss.setBetx(17.02748544);
        twiss.setAlfx(0.4583574683);
        twiss.setDx(-0.3408152943);
        twiss.setDpx(0.01307653962);
        twiss.setBety(123.9323528);
        twiss.setAlfy(-3.422196857);
        twiss.setDy(0.0);
        twiss.setDpy(0.0);

        /*
         * Q20 (MSE as in machine) - Q26 position at extraction (SPS)
         */
        twiss.setX(0.0);
        twiss.setPx(0.0);

        return twiss;

    }

}