/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.factory.BeamFactory;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.NONE;
import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;
import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.RESOURCE;

/**
 * The model definition for Ti8+LHC sector 78
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 */
public class R2018Q26Ti8LhcModelDefinitionFactory extends AbstractR2018Q26Ti8Factory implements ModelDefinitionFactory {

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TI8-LHC Q26 2018");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setResourceOffset("lhctransfer/r2017");
        modelDefinition.setModelPathOffsets(offsets);

        modelDefinition.addInitFile(new CallableModelFileImpl("ti8/sequence/ti8.seq", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(
                new CallableModelFileImpl("lhc/sequence/lhcb4_study_20150119.seq", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("ti8/join-ti8-lhc.madx", ModelFileLocation.RESOURCE));

        OpticsDefinition opticsDefinition = new OpticsDefinitionImpl("LHCB2Transfer-Q26-ATS-2018",
                new ModelFile[] { new CallableModelFileImpl("ti8/strength/TI8_Q26.v2018.str", RESOURCE, STRENGTHS),
                        new CallableModelFileImpl("lhc/strength/ats_11m_fixQ8L4.madx", RESOURCE, STRENGTHS),
                        new CallableModelFileImpl("ti8/set_lhc_flags_to_zero.madx", RESOURCE, NONE), });
        modelDefinition.setDefaultOpticsDefinition(opticsDefinition);

        /* NOTE: sequenceName must correspond to the name in .seq - file! */
        SequenceDefinitionImpl ti8LhcSeqDef = new SequenceDefinitionImpl("ti8lhcb2",
                BeamFactory.createDefaultLhcBeam());
        modelDefinition.addSequenceDefinition(ti8LhcSeqDef);
        modelDefinition.setDefaultSequenceDefinition(ti8LhcSeqDef);

        RangeDefinitionImpl b2range = new RangeDefinitionImpl(ti8LhcSeqDef, "ALL", createTi8Twiss());
        /* XXX invert the X-bpms */
        b2range.addMonitorInvertFilter(new RegexNameFilter("(?i)^BPM.*\\.B2", JMadPlane.H));
        b2range.addMonitorInvertFilter(new RegexNameFilter("(?i)^BTV.*", JMadPlane.H));
        /* invert the x-correctors */
        b2range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MCB.*\\.B2", JMadPlane.H));
        b2range.addPostUseFile(new CallableModelFileImpl("ti8/set-errors-q26.madx", ModelFileLocation.RESOURCE));
        ti8LhcSeqDef.addRangeDefinition(b2range);
        ti8LhcSeqDef.setDefaultRangeDefinition(b2range);
        return modelDefinition;

    }

}
