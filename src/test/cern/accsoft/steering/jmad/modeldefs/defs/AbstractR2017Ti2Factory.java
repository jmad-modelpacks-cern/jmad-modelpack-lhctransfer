/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;

/**
 * @author kfuchsbe
 */
public class AbstractR2017Ti2Factory {

    /**
     * Twiss initial conditions as provided by malika on Oct, 20 2009 betx= 17.02748544, alfx= 0.4583574683,
     * bety=123.9323528, alfy=-3.422196857, dx = -0.3408152943, dpx = 0.01307653962;
     */
    protected final TwissInitialConditionsImpl createTi2Twiss() {
        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl("ti2-twiss");

        twiss.setDeltap(0.0);
        twiss.setBetx(27.77906807);
        twiss.setAlfx(0.63611880);
        twiss.setDx(-0.59866300);
        twiss.setDpx(0.01603536);
        twiss.setBety(120.39920690);
        twiss.setAlfy(-2.70621900);
        twiss.setDy(0.0);
        twiss.setDpy(0.0);

        /*
         * Q20 (MSE as in machine) - Q26 position at extraction (SPS)
         */
        twiss.setX(0.281374274222 - 0.277702013);
        twiss.setPx(0.008696204806 - 0.008874135741);

        return twiss;

    }

}