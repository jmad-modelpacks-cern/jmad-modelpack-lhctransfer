/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.factory.BeamFactory;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * The model definition for Ti2+LHC sector 23
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 */
public class R2015Ti2LhcModelDefinitionFactory implements ModelDefinitionFactory {

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TI2-LHC 2015");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setResourceOffset("lhctransfer/r2015");
        modelDefinition.setModelPathOffsets(offsets);

        modelDefinition.addInitFile(new CallableModelFileImpl("ti2/sequence/ti2.seq", ModelFileLocation.RESOURCE));
        // modelDefinition.addInitFile(new CallableModelFileImpl("ti2/aperture/ti2.aperture.seq",
        // ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("lhc/sequence/lhc2015.seq", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("ti2/join-ti2-lhc.madx", ModelFileLocation.RESOURCE));

        OpticsDefinition opticsDefinition = new OpticsDefinitionImpl("LHCB1Transfer-LHC-2015", new ModelFile[] {
                new CallableModelFileImpl("ti2/strength/ti2.str", ModelFileLocation.RESOURCE, ParseType.STRENGTHS),
                new CallableModelFileImpl("lhc/strength/opt_inj_std.madx", ModelFileLocation.RESOURCE,
                        ParseType.STRENGTHS) });
        modelDefinition.setDefaultOpticsDefinition(opticsDefinition);

        /* NOTE: sequenceName must correspond to the name in .seq - file! */
        SequenceDefinitionImpl ti2LhcSeqDef = new SequenceDefinitionImpl("ti2lhcb1", BeamFactory.createDefaultLhcBeam());
        modelDefinition.addSequenceDefinition(ti2LhcSeqDef);
        modelDefinition.setDefaultSequenceDefinition(ti2LhcSeqDef);

        RangeDefinitionImpl b1range = new RangeDefinitionImpl(ti2LhcSeqDef, "ALL", createTi2Twiss());
        ti2LhcSeqDef.setDefaultRangeDefinition(b1range);
        // b1range.setBeamNumber(BeamNumber.BEAM_1);
        // b1range.addResponeElementNameRegexp("BPM.*");
        // b1range.addResponeElementNameRegexp("BPCK.*");
        /* invert first correctors */
        b1range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MDLH.*", JMadPlane.H));
        b1range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MDLV.*", JMadPlane.V));

        b1range.addPostUseFile(new CallableModelFileImpl("ti2/set-errors-b2-b3.madx", ModelFileLocation.RESOURCE));

        return modelDefinition;

    }

    /**
     * Twiss initial conditions for transferline Ti2
     */
    private final TwissInitialConditionsImpl createTi2Twiss() {
        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl("ti2-twiss");

//        twiss_start_Q20: beta0, betx=27.77906807,bety=120.39920690,
//                alfx=0.63611880,alfy=-2.70621900,
//                dx=-0.59866300,dpx=0.01603536;

        /*
         * original values
         */

        twiss.setDeltap(0.0);
        twiss.setBetx(27.77906807);
        twiss.setAlfx(0.63611880);
        twiss.setDx(-0.59866300);
        twiss.setDpx(0.01603536);
        twiss.setBety(120.39920690);
        twiss.setAlfy(-2.70621900);
        twiss.setDy(0.0);
        twiss.setDpy(0.0);


        return twiss;

    }

}
