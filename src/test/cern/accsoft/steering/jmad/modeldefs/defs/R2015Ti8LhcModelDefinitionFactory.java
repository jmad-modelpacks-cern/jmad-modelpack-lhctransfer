/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.factory.BeamFactory;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * The model definition for Ti8+LHC sector 78
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 */
public class R2015Ti8LhcModelDefinitionFactory implements ModelDefinitionFactory {

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TI8-LHC 2015");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setResourceOffset("lhctransfer/r2015");
        modelDefinition.setModelPathOffsets(offsets);

        modelDefinition.addInitFile(new CallableModelFileImpl("ti8/sequence/ti8.seq", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(
                new CallableModelFileImpl("lhc/sequence/lhcb4_study_20150119.seq", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("ti8/join-ti8-lhc.madx", ModelFileLocation.RESOURCE));

        OpticsDefinition opticsDefinition = new OpticsDefinitionImpl("LHCB2Transfer-LHC-2015",
                new ModelFile[] {
                        new CallableModelFileImpl("ti8/strength/ti8.str", ModelFileLocation.RESOURCE,
                                ParseType.STRENGTHS),
                        new CallableModelFileImpl("lhc/strength/opt_inj_std.madx", ModelFileLocation.RESOURCE,
                                ParseType.STRENGTHS) });
        modelDefinition.setDefaultOpticsDefinition(opticsDefinition);

        /* NOTE: sequenceName must correspond to the name in .seq - file! */
        SequenceDefinitionImpl ti8LhcSeqDef = new SequenceDefinitionImpl("ti8lhcb2",
                BeamFactory.createDefaultLhcBeam());
        modelDefinition.addSequenceDefinition(ti8LhcSeqDef);
        modelDefinition.setDefaultSequenceDefinition(ti8LhcSeqDef);

        RangeDefinitionImpl b2range = new RangeDefinitionImpl(ti8LhcSeqDef, "ALL", createTi8Twiss());
        /* XXX invert the X-bpms */
        b2range.addMonitorInvertFilter(new RegexNameFilter("(?i)^BPM.*\\.B2", JMadPlane.H));
        b2range.addMonitorInvertFilter(new RegexNameFilter("(?i)^BTV.*", JMadPlane.H));
        /* invert the x-correctors */
        b2range.addCorrectorInvertFilter(new RegexNameFilter("(?i)^MCB.*\\.B2", JMadPlane.H));
        b2range.addPostUseFile(new CallableModelFileImpl("ti8/set-errors-b2-b3.madx", ModelFileLocation.RESOURCE));
        ti8LhcSeqDef.addRangeDefinition(b2range);
        ti8LhcSeqDef.setDefaultRangeDefinition(b2range);
        return modelDefinition;

    }

    private final TwissInitialConditionsImpl createTi8Twiss() {
        TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl("ti8-twiss");

        // beta_Q20: beta0, betx = 27.93108087 , alfx = 0.65054900,
        // bety = 120.05843813, alfy =-2.70513448,
        // dx= -0.58140484, dpx= 0.01446870;
        //
        // beta_Q20_meas: beta0, betx =27.93108087,bety = 120.05843813,
        // alfx = 0.65054900, alfy =-2.70513448,
        // dx= -0.51953884,dpx= 0.01357203,
        // dy=0.07474805, dpy= -0.00011898;

        twiss.setDeltap(0.000);
        twiss.setBetx(27.93108087);
        twiss.setAlfx(0.65054900);
        twiss.setDx(-0.58140484);
        twiss.setDpx(0.01446870);
        twiss.setBety(120.05843813);
        twiss.setAlfy(-2.70513448);
        twiss.setDy(0.0);
        twiss.setDpy(0.0);

        return twiss;
    }

}
